import pytest
import mock
from pymza.state import TaskState



def test_get_loads_data_from_manager():
    mgr = mock.Mock()
    mgr.get_key.return_value = 123

    state = TaskState(mgr)

    v = state['some_key']

    mgr.get_key.assert_called_once_with('some_key')
    assert v == 123


def test_set_puts_data_to_manager():
    mgr = mock.Mock()

    state = TaskState(mgr)

    state['some_key'] = '1'

    assert mgr.commit_state.called == False


def test_commit():
    mgr = mock.Mock()

    state = TaskState(mgr)

    state['some_key'] = '1'
    state.commit()

    assert mgr.commit_state.called == True


def test_commit_resets_cache():
    mgr = mock.Mock()

    state = TaskState(mgr)

    state['some_key'] = '1'

    assert len(state._cache) > 0

    state.commit()

    assert len(state._cache) == 0


def test_delete_works():
    mgr = mock.Mock()
    state = TaskState(mgr)

    del state['some_key']

    state.get('some_key')

    assert mgr.get_key.called == False


def test_get_after_set_takes_data_from_cache():
    mgr = mock.Mock()
    state = TaskState(mgr)

    state['some_key'] = 556
    assert state.get('some_key') == 556

    assert mgr.get_key.called == False


def test_set_after_delete_works():
    mgr = mock.Mock()
    state = TaskState(mgr)

    del state['some_key']
    state['some_key'] = 123

    assert state['some_key'] == 123

    assert mgr.get_key.called == False


def test_range_reads_data_from_mgr():
    mgr = mock.Mock()
    state = TaskState(mgr)

    mgr.iterate_key_values.return_value = iter([('a', 1), ('b', 2)])

    values = list(state.range(key_from='a', key_to='z'))

    mgr.iterate_key_values.assert_called_once_with(key_from='a', key_to='z')
    assert values == [('a', 1), ('b', 2)]


@pytest.mark.xfail
def test_range_merges_data_in_cache_with_data_from_manager():
    mgr = mock.Mock()
    state = TaskState(mgr)

    mgr.iterate_key_values.return_value = iter([('a', 1), ('b', 2), ('c', 3), ('z', 4)])

    state['b'] = 3
    state['0'] = 1
    state['g'] = 5
    values = list(state.range(key_from='a', key_to='z'))

    mgr.iterate_key_values.assert_called_once_with(key_from='a', key_to='z')
    assert values == [('a', 1), ('b', 3), ('c', 3), ('g', 5), ('z', 4)]

# test enumeration returns data in cache
# flushing data from cache when it is big enough